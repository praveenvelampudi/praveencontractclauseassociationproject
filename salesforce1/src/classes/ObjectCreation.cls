/*This class acts as the Data repository for the test classes and creates all the required objects
 * for the test class methods */
public class ObjectCreation {
    
    //Creates the Account object
    public Account accountCreation(){
        Account acc = new account();
        acc.name='test account';
        insert acc;
        return acc;
    }
    
    //Creates the Contract object
    public Contract contractCreation(){
        Account acc= accountcreation();
        Contract con = new contract();
       	con.AccountId= acc.id;
        con.Name='test contract';
        con.Status='draft';
        con.StartDate= Date.newInstance(2019, 01, 15);
        con.ContractTerm= 4;
        insert con;
        return con;
    }
    
    //Creates the Clause object
    public Clause__C clauseCreation(){
        Clause__c cla = new Clause__c();
        cla.Name ='test clause';
        cla.Type__c='other';
        cla.Description__c='This is a test clause';
        insert cla;
        return cla;
    }
    
    //Creates the ContractClauseAssociation object
    public ContractClauseAssociation__c associationCreation(){
        Contract con= contractCreation();
        Clause__c cla= clauseCreation();
        ContractClauseAssociation__c contcla = new ContractClauseAssociation__c();
        contcla.Name='test association';
        contcla.Contract__c=con.id;
        contcla.Clause__c= cla.id;
        insert contcla;
        return contcla;
    }

}