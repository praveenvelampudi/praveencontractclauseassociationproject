@istest(seealldata=false)
public class ProjectControllerTest{
    //Creating the instance of the Standard Controller for the Project controller Constructors
   		static Contract contractcon = new contract();
        static ApexPages.StandardController testconstructor = new ApexPages.StandardController(contractcon);
    	static List<Contract> contractList = new List<contract>();
        static ApexPages.StandardsetController testconstructor1 = new ApexPages.StandardsetController(contractList);
    
    //this method Tests the redirect page and checks if the redirect pages are matching 
    public static testmethod void testredirectpage(){
        ObjectCreation test1 = new ObjectCreation();
        ContractClauseAssociation__c contcla= test1.associationCreation();
        
        Test.startTest();
        	ApexPages.currentPage().getParameters().put('contractidvalue', String.valueOf(contcla.id));
			ProjectController testcontroller = new ProjectController(testconstructor);
        	PageReference ret  = testcontroller.redirectpage();
            PageReference correct = new PageReference('/apex/ContractClause?core.apexpages.request.devconsole=1&id='+contcla.id);
        	System.assertEquals(ret.getUrl(), correct.getUrl());
        Test.stopTest();
        
    }
    
    //this method tests the closing of the modal window, that is opened while making a new Contract Clause Association Object
    //here we assert whether the displaypopup is false
	public static testmethod void testclosePopup(){
        Test.startTest();
        	ProjectController testcontroller = new ProjectController(testconstructor);
        	testcontroller.showPopup();
        	testcontroller.closePopup();
        	System.assertEquals(false,testcontroller.displayPopup );
        Test.stopTest();
    }
    
    //This method tests the remove function or delete functionality of the Contract Clause Association objects
    //we assert if the id is present or not
    public static testmethod void testRemoveval(){
        ObjectCreation test1 = new ObjectCreation();
        ContractClauseAssociation__c contcla= test1.associationCreation();
        
        Test.startTest();
        	ProjectController testcontroller = new ProjectController(testconstructor);
        	//obtains the id for the Contract details page
        	ApexPages.currentPage().getParameters().put('contractidvalue', String.valueOf(contcla.Contract__r.id));
        	//obtains the id for the association object
        	ApexPages.currentPage().getParameters().put('associationidvalue', String.valueOf(contcla.id));
        	PageReference ret = testcontroller.Removeval();
        	Boolean flag = false;
        	try{
                ContractClauseAssociation__c clauseid = [select id from ContractClauseAssociation__c where id=:contcla.id];  
            }
            catch(System.QueryException e){
                flag= true;
            }
        	System.assertEquals(true, flag);
        Test.stopTest();  
    }
    
    //This method is a negative test for the removal or delete functionality of the Association object
    //Here, we create a bad sql query and hence pass the wrong id. 
    //We assert that the record does not get deleted
    public static testmethod void testRemovevalnegative(){
        ObjectCreation test1 = new ObjectCreation();
        ContractClauseAssociation__c contcla= test1.associationCreation();
        
        Test.startTest();
        	ProjectController testcontroller = new ProjectController(testconstructor);
        	//obtains the id for the Contract details page
        	ApexPages.currentPage().getParameters().put('contractidvalue', String.valueOf(contcla.Contract__r.id));
       	    //obtains the wrong id for the association object
        	ApexPages.currentPage().getParameters().put('id', String.valueOf(contcla.id));
        	PageReference ret = testcontroller.Removeval();
        	Boolean flag = false;
        	try{
                ContractClauseAssociation__c clauseid = [select id from ContractClauseAssociation__c where id=:contcla.id];  
            }
            catch(System.QueryException e){
                flag= true;
            }
        	System.assertEquals(false, flag);
        Test.stopTest();  
    }
    
    //This method allows us to the test the population of the list of association objects corresponding to a particular contract
    //here we assert the size of the list that is obtained.
    public static testmethod void testgetPopulation(){
        //Creation of multiple association objects
        ObjectCreation test1 = new ObjectCreation();
        ContractClauseAssociation__c contcla1 = test1.associationCreation();
        ContractClauseAssociation__c contcla2 = test1.associationCreation();
        ContractClauseAssociation__c contcla3 = test1.associationCreation();
        
        //mapping of the contracts of all the association objects to a single contract
        contcla2.contract__c=contcla1.contract__c;
        contcla3.contract__c=contcla1.contract__c;
        upsert contcla2;
        upsert contcla3;
        
        Test.startTest();
        	ProjectController testcontroller = new ProjectController(testconstructor);
        	contract contractID= [select id from contract where id=:contcla1.Contract__c];
        	System.debug(contractID);
        	ApexPages.currentPage().getParameters().put('id', String.valueOf(contractID.id));
            List<ContractClauseAssociation__c> clauses = testcontroller.getPopulation();
        	System.assertEquals(3, clauses.size());
        Test.stopTest();   
    }
    
    //This method allows us to test whether the save functionality of the association objects.
    //Here we assert that when a new association object is created the id exisits 
    public static testmethod void testsaveval(){
       Test.startTest();
            ObjectCreation test1 = new ObjectCreation();
            ContractClauseAssociation__c contcla= test1.associationCreation();
            ProjectController testcontroller = new ProjectController(testconstructor);
            ApexPages.currentPage().getParameters().put('contractidvalue', String.valueOf(contcla.Contract__r.id));
            testcontroller.showPopup();
        	//attaching the association object of controller to created association object
            testcontroller.conc = contcla;
            PageReference ret = testcontroller.saveval();
            System.assertEquals(false, testcontroller.displayPopup);
            Boolean flag = true;
        	try{
                ContractClauseAssociation__c clauseid = [select id from ContractClauseAssociation__c where id=:contcla.id];
                
            }
            catch(System.QueryException e){
                flag= false;
            }
        	System.assertEquals(true, flag);  
        Test.stopTest();
    }
    
    //This Method is a negative test for the saving method. 
    //Here we try to create a DML exception but not populating all the required fields for insert operation 
    //of the new association object. 
    public static testmethod void testsavevalnegative(){
       Test.startTest();
            ObjectCreation test1 = new ObjectCreation();
            ContractClauseAssociation__c contcla= test1.associationCreation();
        	//Setting the contract field to null in order to obtain a DML Exception
        	contcla.Contract__c=null;
            ProjectController testcontroller = new ProjectController(testconstructor);
            ApexPages.currentPage().getParameters().put('contractidvalue', String.valueOf(contcla.Contract__r.id));
            testcontroller.showPopup();
            testcontroller.conc = contcla;
            PageReference ret = testcontroller.saveval();
            System.assertEquals(false, testcontroller.displayPopup);
            Boolean flag = true;
        	try{
                ContractClauseAssociation__c clauseid = [select id from ContractClauseAssociation__c where id=:contcla.id];
                
            }
            catch(System.QueryException e){
                flag= false;
            }
        	System.assertEquals(true, flag);  
        Test.stopTest();
    }
    
    //This method allows us to test whether the save functionality of the contract objects.
    //Here we assert that when a new contract object is created the id exisits
    public static testmethod void testsavevalContract(){
       Test.startTest();
            ObjectCreation test1 = new ObjectCreation();
            Contract cont= test1.contractCreation();
            ProjectController testcontroller1 = new ProjectController(testconstructor1);
            ApexPages.currentPage().getParameters().put('contractidvalue', String.valueOf(cont.id));
            testcontroller1.showPopupContract();
            testcontroller1.newcon = cont;
            PageReference ret = testcontroller1.savevalContract();
            System.assertEquals(false, testcontroller1.displayPopupContract);
            Boolean flag = true;
        	try{
                Contract contractid = [select id from Contract where id=:cont.id];
                
            }
            catch(System.QueryException e){
                flag= false;
            }
        	System.assertEquals(true, flag);  
        Test.stopTest();
    }
    
    //This method tests the remove function or delete functionality of the Contract objects
    //we assert if the id is present or not
    public static testmethod void testRemovevalContract(){
        ObjectCreation test1 = new ObjectCreation();
        Contract cont= test1.contractCreation();
        
        Test.startTest();
        	ProjectController testcontroller1 = new ProjectController(testconstructor1);
        	ApexPages.currentPage().getParameters().put('contractidvalue', String.valueOf(cont.id));
        	PageReference ret = testcontroller1.RemovevalContract();
        	Boolean flag = false;
        	try{
                Contract contractid = [select id from Contract where id=:cont.id];
                
            }
            catch(System.QueryException e){
                flag= true;
            }
        	System.assertEquals(true, flag);
        Test.stopTest();
    }
    
     //This method is a negative test for the removal or delete functionality of the Contract object
    //Here, we create a bad sql query and hence pass the wrong id. 
    //We assert that the record does not get deleted
    public static testmethod void testRemovevalContractnegative1(){
        ObjectCreation test1 = new ObjectCreation();
        Contract cont= test1.contractCreation();
        
        Test.startTest();
        	ProjectController testcontroller1 = new ProjectController(testconstructor1);
        	//we pass the wrong id here
        	ApexPages.currentPage().getParameters().put('id', String.valueOf(cont.id));
        	PageReference ret = testcontroller1.RemovevalContract();
        	Boolean flag = false;
        	try{
                Contract contractid = [select id from Contract where id=:cont.id];   
            }
            catch(System.QueryException e){
                flag= true;
            }
        	System.assertEquals(false, flag);
        Test.stopTest();    
    }
    
    //this method tests the closing of the modal window, that is opened while making a new Contract Object
    //here we assert whether the displaypopupContract is false
    public static testmethod void testclosePopupContract(){
        Test.startTest();
        	ProjectController testcontroller1 = new ProjectController(testconstructor1);
        	testcontroller1.showPopupContract();
        	testcontroller1.closePopupContract();
        	System.assertEquals(false,testcontroller1.displayPopupContract );
        Test.stopTest();
    }
   
    //This method allows us to test the functionality of the Back button in order to go back to the page containing the list
    //of contract objects. Here we assert if the url is equal.
    public static testmethod void testGoBack(){
       ProjectController testcontroller1 = new ProjectController(testconstructor1);
       ObjectCreation test1 = new ObjectCreation();
       PageReference ret = testcontroller1.goBack();
       PageReference correct = new PageReference('https://c.na85.visual.force.com/apex/ListOfContracts?core.apexpages.request.devconsole=1');
       System.assertEquals(correct.getUrl(), ret.getUrl());
    }
    
    //This method allows us to test the redirction functionality, when we try to redirect to the clause details 
    //page by clicking on the name of the clause.
     public static testmethod void testclauseRedirect(){
       ObjectCreation test1 = new ObjectCreation();
        ContractClauseAssociation__c contcla= test1.associationCreation();
        
        Test.startTest();
        	ApexPages.currentPage().getParameters().put('id', String.valueOf(contcla.clause__c));
        	clause__c clauseid = [select id from Clause__c where id=:contcla.clause__c];
			ProjectController testcontroller = new ProjectController(testconstructor);
        	PageReference ret  = testcontroller.clauseRedirect();
            PageReference correct = new PageReference('/'+clauseid.id);
        	System.assertEquals(ret.getUrl(), correct.getUrl());
        Test.stopTest();
                
    }
    
    //This Method is a negative test for the saving method of contract. 
    //Here we try to create a DML exception but not populating all the required fields for insert operation 
    //of the new contract object. 
    public static testmethod void testsavevalContractnegative(){
       Test.startTest();
            ObjectCreation test1 = new ObjectCreation();
            Contract cont= test1.contractCreation();
            cont.AccountId=null;
            ProjectController testcontroller1 = new ProjectController(testconstructor1);
            ApexPages.currentPage().getParameters().put('contractidvalue', String.valueOf(cont.id));
            testcontroller1.showPopupContract();
            testcontroller1.newcon = cont;
            PageReference ret = testcontroller1.savevalContract();
            System.assertEquals(false, testcontroller1.displayPopupContract);
            Boolean flag = true;
        	try{
                Contract contractid = [select id from Contract where id=:cont.id];
                
            }
            catch(System.QueryException e){
                flag= false;
            }
        	System.assertEquals(true, flag);  
        Test.stopTest();
    } 
}