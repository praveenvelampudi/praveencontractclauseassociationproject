/* This class acts as an extention controller for the use case of Contract Clause Association
 * It controls the rediretion of the various pages and the population of the list of objects
*/

public class ProjectController {
   
    public id contractid;	//The Id of the contact
    public id clauseid;		//The ID of the clause
    public boolean displayPopup {get; set;} //getter and setter for the popup
    public boolean displayPopupContract{get;set;}
    public ContractClauseAssociation__c conc{get;set;} //getter and setter for the Contract clause junction object
    public contract newcon{get;set;}//getter and setter for the contract object
    //Constructor for the ListOfContacts VF page
    public projectController(ApexPages.StandardsetController controller){
        contractid= ApexPages.currentPage().getParameters().get('Id');
    }
    
    //Constructor for the ContractClause VF page, collects the ID of the contract and clause
     public projectController(ApexPages.StandardController controller){
        contractid= ApexPages.currentPage().getParameters().get('id');
        clauseid= ApexPages.currentPage().getParameters().get('id');
    }
 
    //This method is used for the redirection of the page from the ListOfContracts to ContractClause page upon 
	//select/click of a contract. It returns a pagereference used for redirection.  
    public PageReference redirectpage (){
        PageReference pageRef ;
        contractid= ApexPages.currentPage().getParameters().get('contractidvalue');
        if(contractid!=null){
        	pageRef = new PageReference('/apex/ContractClause?core.apexpages.request.devconsole=1&id='+contractid);	
       	}
        return pageRef;
    }
    
    //This method obtains the list of ContractClauseAssociation values for the selected contract
    public list<ContractClauseAssociation__c> getPopulation(){
        contractid= ApexPages.currentPage().getParameters().get('id');
        list<ContractClauseAssociation__c> clausevalues= [select Name, clause__r.name,contract__r.name, id, clause__r.Description__c, clause__r.Type__c from ContractClauseAssociation__c  where contract__r.id =:contractid];
        return clausevalues;
    }
    
    //This method allows the delete functionality on the ContractClauseAssociation object
    public PageReference Removeval(){
        id associationid = ApexPages.currentPage().getParameters().get('associationidvalue');
        try {
        	ContractClauseAssociation__c remove = [select id from ContractClauseAssociation__c where id =:associationid ];
        	delete remove;
        }
        catch(System.QueryException e){
            System.debug(e.getMessage());
        }
       	return new PageReference('/apex/ContractClause?id='+contractid);
    }
    
    //method allows for the closure of the modal window
    public void closePopup(){        
        displayPopup = false;    
    }  
    
    //method allows for the opening of the modal window
    //it also initializes the ContractClauseAssociation object inorder to create a new record
    public void showPopup(){        
        displayPopup = true;
        conc = new ContractClauseAssociation__c ();
        conc.contract__c= contractid;
    }
    
    //This method allows for the saving of the new record that is created on the ContractClauseAssociation object
    //to be inserted. It then refreshes the page. 
    public PageReference saveval(){
        try{
        	upsert conc;
        }
        catch(System.DmlException e){
            System.debug(e.getMessage());
        }
        displayPopup = false;
        return new PageReference('/apex/ContractClause?id='+contractid);    
    }
    
    //This method allows the user to redirct to the List of Contracts from the Detailed contract page
    public PageReference goBack(){
        return new PageReference('https://c.na85.visual.force.com/apex/ListOfContracts?core.apexpages.request.devconsole=1');
    }
    
    //This method allows the redirction of the page to view the current clause in the out of box feature
    public PageReference clauseRedirect(){
        PageReference pageRef ;
        clauseid= ApexPages.currentPage().getParameters().get('id');
        System.debug(clauseid);
       	pageRef = new PageReference('/'+clauseid);
        return pageRef;
    }
    
    //method allows for the closure of the modal window for New contract 
    public void closePopupContract(){        
        displayPopupContract = false;    
    }  
    
    //method allows for the opening of the modal window for new contract
    //it also initializes the Contract object inorder to create a new record
    public void showPopupContract(){        
        displayPopupContract = true;
        newcon = new Contract();
        newcon.id= contractid;
        newcon.Status='draft';
    }
    
    //This method allows the saving of the new contract that is created, it then refreshes the list of contracts
    public PageReference savevalContract(){
        newcon.Status='draft';
        try{
        	upsert newcon;
        }
        catch(System.DmlException e){
            System.debug(e.getMessage());
        }
        displayPopupContract = false;
        return new PageReference('https://c.na85.visual.force.com/apex/ListOfContracts?core.apexpages.request.devconsole=1');    
    }
    
    //This method allows the user to delete an exisiting contract
    public PageReference RemovevalContract(){
        id contractid = ApexPages.currentPage().getParameters().get('contractidvalue');
        try{
        	Contract remove = [select id from Contract where id =:contractid ];
        	delete remove;
        }
        catch(System.QueryException e){
            System.debug(e.getMessage());
        }
       	return new PageReference('https://c.na85.visual.force.com/apex/ListOfContracts?core.apexpages.request.devconsole=1');
    }
}