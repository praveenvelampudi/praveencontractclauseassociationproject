The following file explains the process in which I have solved the given project usecase.

Tools Used:
Intelli J IDE /
BIT Bucket version control /
SourceTree /
Force.com Dev console /

Approach 1: Using out of box features:

Here, we have created all the requirements of the project using the inbuilt salesforce out of box features. 
The screenshots that display the look and feel of this method of implementation have been provided in the folder
"Outofbox ScreenShots". 

Approach 2: Using Visual Force Lightning Design

Here, we have created all the requirements of the project using Visual Force with appearence of Lightning Design. 
The screenshots that display the look and feel of this method of implementation have been provided in the folder
"Custom UI ScreenShots".

Along with screenshots of the final product, the code used for the project can be found in the "salesforce1" folder. 
The folder contains the Apex classes, Apex Pages, TestClasses, Data Repository, and static resources used to make the project.